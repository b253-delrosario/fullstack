const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", printFullName);
txtLastName.addEventListener("keyup", printFullName);

function printFullName(event){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};

// ===================================================================== //
// ======================= Miss Janie's Solution ======================= //
/*


const updateFullName = () => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);


*/
// ===================================================================== //

const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName)

labelFirstName.addEventListener("click", event => {
	console.log(event);
	console.log(event.target);
	alert("You clicked the First Name label!");
});

const labelLastName = document.querySelector("#label-last-name");
console.log(labelLastName)

labelLastName.addEventListener("click", event => {
	console.log(event);
	console.log(event.target);
	alert("You clicked the Last Name label!");
});
