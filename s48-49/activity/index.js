// alert("Guten tag!");

// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON formant.

/*

	Syntax:
		fetch("url", {options})


	url - this is the address which the request is to be made and source where the response will come from (endpoint)

	options - array or properties that contains the HTTP method, body of request and headers.

*/

// Get Post Data / Retrieve Function
fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => response.json())
	.then(data => showPosts(data));

// VIEW POST - used to display each post from JSON placeholder.
const showPosts = (posts) => {

	//Create a variable that will contain all the posts
	let postEntries = "";

	// To iterate each posts from the API
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	}); 

	// To check what is stored in the postEntries variables.
	console.log(postEntries);

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries;	
}

/*
	Mini-Activity:
		Retrieve a single post from JSON API and print it in the console.
*/

fetch("https://jsonplaceholder.typicode.com/posts/50")
	.then((response) => response.json())
	.then((data) => console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/25")
	.then((response) => response.json())
	.then((data) => console.log(data));

// Post Data / Create Function
document.querySelector("#form-add-post").addEventListener("submit", e => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", 
		{
			method: "POST",
			body: JSON.stringify({
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 1
			}),
			headers: {
				"content-type": "application/json"
			}
		}).then(response => response.json())
			.then(data => {
				console.log(data);
				alert("Post Successfully Added!");
			});
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
})


// EDIT POST DATA/ Edit Button
const editPost = (id) => {

	// Displayed Post in the Post Section (Source of data)
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update Post Data / PUT Method

document.querySelector("#form-edit-post").addEventListener("submit", e => {
	e.preventDefault();

	let id = document.querySelector("#txt-edit-id").value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
		{
			method: "PUT",
			body: JSON.stringify({
				id: id,
				title: document.querySelector("#txt-edit-title").value,
				body: document.querySelector("#txt-edit-body").value,
				userId: 1
			}),
			headers: {
				"content-type": "application/json"
			}
		}).then(res => res.json())
			.then(data => {
				console.log(data);
				alert("Post Successfully Updated!");
			});

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
});


// ============================= ACTIVITY ============================= //

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: "DELETE"});
  document.querySelector(`#post-${id}`).remove();
}
